-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
                  ___  _____          ___  _____                  ___
  |        /\    /       |           /       |      /\    |\   | |   \
  |       /  \   \___    |           \___    |     /  \   | \  | |    \
  |      /____\      \   |               \   |    /____\  |  \ | |    /
  |____ /      \  ___/   |            ___/   |   /      \ |   \| |___/

-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

Index:
------
1. Author information
2. Program description
3. Installation and requirements
4. Instructions
5. Files included in zip
6. Date released and version number
7. Changelog
8. Contribute
9. Support
0. Copyright


=======================
 1. Author information
=======================

This program was made and revised by Mark Lisi�i� #markman4897
- eMail: lisicic.mark@gmail.com
- GitHub: https://github.com/markman4897
- BitBucket: https://bitbucket.org/markman4897/
- website: http://84.41.40.10:2048/


========================
 2. Program description
========================

Basic description:
------------------
This program is an imitation of old arcade game developed a bit further so it looks a bit more modern. The program 
runs on Love2D engine and is written in programing language called Lua.

The program was made for a school assignment in year 2015/2016.

Backstory of the game:
----------------------
The backstory in this game is that the Earth got invaded by bad aliens who want to exterminate all living things on 
the planet so they can mine its resources. Because they are the more developed ones and can travel in spaceships, 
they got the upper hand on us humans. There is only one more hope of defending our homeplanet from the aliens, and 
that is with the help of crazy scientists new mounted railgun. It spans around the last free land on earth, the tech
development center where the crazy scientist Gefistov built his newest invention for long range anti-air protection.
You are his trusty assistant, a Vietnam war veteran, upon whom he entrusted the job to man his railgun and protect 
what is left of Earth.

Game:
-----
You operate the railgun and have to shoot down the incomming aliens before they can reach you and wreak havoc upon 
the last of humanity's lands.


==================================
 3. Installation and requirements
==================================

There are no requirements for this game.
There is no installation, just follow the instructions in paragraph "4. Instructions" .


=================
 4. Instructions
=================

Windows:
--------
1. Run the "LastStand.exe" file, located in folder "love-0.10.1-win32", by double clicking it and the game will open 
   up.

Linux:
------
1. Install Love rep
2. Open terminal and change directory to the folder where this file is located (with "cd" command).
3. Run command "love ." and the game will open up.

Commands:
---------
When the game opens, you will be on the main screen. Press any button to advance into the game.

When in game you will see the railgun on the left (spheric white objects that points to the right). You can move the 
railgun up and down with "arrow up" and "arrow down" key. You can shoot with "S" key.

When you finish the game (die) you return to the main screen where you can once more press any button to advance into 
game from start.

While in game you can press the "D" key for debug mode. This will display circles around moving objects and also 
enemies count, bullets count, enemy spawn rate and FPS.


==========================
 5. Files included in zip
==========================

.
+-- assets
|   +-- background.gif
|   +-- background.jpg
|   +-- background.png
|   +-- background.tga
|   +-- background.xcf
|   +-- bullet.gif
|   +-- bullet.jpg
|   +-- bullet.png
|   +-- bullet.tga
|   +-- enemy.gif
|   +-- enemy.jpg
|   +-- enemy.png
|   +-- enemy.tga
|   +-- explode.ogg
|   +-- font.ttf
|   +-- game.ogg
|   +-- menu.ogg
|   +-- player.gif
|   +-- player.jpg
|   +-- player.png
|   +-- player.tga
|   +-- shoot.ogg
|   +-- title.gif
|   +-- title.jpg
|   +-- title.png
|   +-- title.tga
|   +-- title.xcf
+-- love-0.10.1-win32
|   +-- changes.txt
|   +-- compile.bat
|   +-- game.ico
|   +-- LastStand.exe
|   +-- LastStand.love
|   +-- licence.txt
|   +-- love.dll
|   +-- love.exe
|   +-- love.ico
|   +-- lua51.dll
|   +-- mpg123.dll
|   +-- msvcp120.dll
|   +-- msvcr120.dll
|   +-- OpenAL32.dll
|   +-- readme.txt
|   +-- SDL2.dll
+-- Autorun.inf
+-- conf.lua
+-- game.lua
+-- LastStand.ico
+-- LastStand.love
+-- main.lua
+-- readme.txt
+-- splash.lua
+-- TODO.txt



=====================================
 6. Date released and version number
=====================================

Date:
-----
20.3.2016

Version:
--------
V1 - beta1.1.0 - release candidate 1

==============
 7. Changelog
==============

* added documentation
* added comments in the code
* revised all the functions


===============
 8. Contribute
===============

I encourage all usage of this programand will accept it into the main program is you so wish if they are in line with 
the project. To contribute your personal work please contact me with any of the previously stated contacts (in Author 
information paragraph).

The source code of this developing project is on this link: https://bitbucket.org/markman4897/laststand/


============
 9. Support
============

I accept any and all donations. If you like the product, feel like you have learned something from it or just enjoy 
using it you can always contact me with any of the previously stated contacts (in Author information paragraph) and 
we will find a way for you to support me.


==============
 0. Copyright
==============

GNU General Public License 2016-2066, the LastStand project.
