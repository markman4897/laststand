splash = {}

function splash.load()
  -- definirana spremenljivka za beleženje časa
  splash.dt_temp = 0
  
  -- ustavi predvajane zvoke
  love.audio.stop()
  -- predvajaj zvok "music.menu"
  love.audio.play(music.menu)
end

-- funkcija za izris komponent pred igro
function splash.draw()
  -- ukaz za izris slike "title" in definirana velikost ter pozicija
  love.graphics.draw(imgs["title"], 1280/2*scale, (splash.dt_temp-1)*32*scale, 0, scale, scale, imgs["title"]:getWidth()/2)
  -- nastavimo barvo za nadaljno uporabo
  love.graphics.setColor(fontcolor.r, fontcolor.g, fontcolor.b)
  -- =show after 2.5s
  -- če je spremenljivka "splash.dt-temp" enaka 2.5 natisnimo besedilo "Press start"
  if splash.dt_temp == 2.5 then
    love.graphics.printf("Press start", 1280/2*scale, 720/2*scale, 0, "center")
  end

  -- =if previous games
  -- če smo prisli do stanja "splash" iz igre izpisi rezultat zadnje igre
  if game.score ~= 0 then
    love.graphics.printf("Score:"..game.score, 1280/2*scale, 720/1.5*scale, 0, "center", 0, 0.8*scale, 0.8*scale)
  end

  -- =reset the color
  -- ponastavimo barvo na belo
  love.graphics.setColor(255,255,255)
end

-- funkcija za merjenje časa
function splash.update(dt)
  -- =update dt_temp
  -- posodobimo spremenljivko za čas
  splash.dt_temp = splash.dt_temp + dt
  -- =wait 2.5s, then stop in place
  -- če je cas vec kot 2.5 potem ga nastavimo nazaj na 2.5
  if splash.dt_temp > 2.5 then
    splash.dt_temp = 2.5
  end
end

-- funkcija ki bere vnos iz tipkovnice
function splash.keypressed(key)
  -- =Change to game state, and init game
  -- če je pritisnjena katera koli tipka nastavimo stanje na "game" in s tem pričnemo novo igro
  state = "game"
  game.load()
end
