debug = false -- spremenljivka, ki pove v katerem načinu izvajanja smo
-- komanda require, ki ne dovoli izvajanje nadaljne kode, če ni dodatnih datotek
require("splash") -- ne nadaljuj izvajanja, če ni datoteke "splash.lua"
require("game") -- ne nadaljuj izvajanja, če ni datoteke "game.lua"

-- funkcija, ki se izvede na začetku, ko odpremo igro
function love.load()
  -- =Load images (global assets)
  -- nalaganje slik v spremenljivke za kasnejšo rabo
  img_fn = {"bullet", "enemy", "player", "title", "background"} -- naloži slike "bullet", "enemy", ... v spremenljivko img_fn
  imgs = {} -- ustvari spremenljivko imgs
  music = {} -- ustvari spremenljivko music
  -- iz spremenljivke imgs_fn pretvori vse v imena datotek in jih zapiše v spremenljivko imgs
  for _,v in ipairs(img_fn) do
    imgs[v] = love.graphics.newImage("assets/"..v..".png") -- prebere zaporeden element spremenljivke img_fn in ji spredaj doda "assets/", zadaj pa ".gif" ter jo zapiše kot zaporedni element spremenljivke imgs
  end

  -- =Set filters to nearest
  -- nastavi filter na "nearest" za vsako sliko v spremenljivki imgs (način kako se slike prikažejo)
  for _,v in pairs(imgs) do
    v:setFilter("nearest", "nearest")
  end

  -- =Play music and loop it
  -- uvozi zvok "menu.ogg" v spremenljivko "music.menu" in jo prebere s tehnologijo "stream"
  music.menu = love.audio.newSource("assets/menu.ogg", "stream")
  -- nastavi zvok "music.menu" da se ponavlja
  music.menu:setLooping(true)

  -- uvozi zvok "game.ogg" v spremenljivko "music.game" in jo prebere s tehnologijo "stream"
  music.game = love.audio.newSource("assets/game.ogg", "stream")
  -- nastavi zvok "music.game" da se ponavlja
  music.game:setLooping(true)

  -- =load shoot sound
  -- uvozi zvok "shoot.ogg" v spremenljivko "shoot" in jo prebere s tehnologijo "static"
  shoot = love.audio.newSource("assets/shoot.ogg", "static")
  -- uvozi zvok "explode.ogg" v spremenljivko "explode" in jo prebere s tehnologijo "static"
  explode = love.audio.newSource("assets/explode.ogg", "static")

  -- =Initialize font, and set it
  -- uvozi font "font.ttf" v spremenljivko "font" z velikostjo 55 * scale (iz "conf.lua")
  font = love.graphics.newFont("assets/font.ttf", 55*scale)
  -- nastavi "font" kot font ki se bo uporabil
  love.graphics.setFont(font)

  -- =define colors
  -- nastavimo spremeljivko "bgcolor" ki bo predstavljala barvo ozadja z RGB vrednostmi
  bgcolor = {r=0, g=51, b=153}
  -- nastavimo spremeljivko "fontcolor" ki bo predstavljala barvo besedila z RGB vrednostmi
  fontcolor = {r=255, g=251, b=151}

  -- =initial state
  -- nastavimo spremenljivko "state" ki predstavlja trenutno stanje aplikacije na "splash"
  state = "splash"
  
  -- =load the splash
  -- naložimo datoteki splash in game
  splash.load()
  game.load()
end

-- funkcija ki bo narisala željene komponente
function love.draw()
  -- =Set color
  -- nastavimo barvo za nadaljno uporabo
  love.graphics.setColor(bgcolor.r, bgcolor.g, bgcolor.b)
  -- =Draw rectangle for background
  -- narišemo poln pravokotnik čez celo okno
  love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
  -- nastavimo barvo na belo (da ne pokvarimo nadaljnih izrisov (če imamo nastavljeno barvo in izrišemo sliko bo barva uporabljena kot filter!))
  love.graphics.setColor(255,255,255)

  -- =call the state's draw function
  -- preverimo v katerem stanju je aplikacija (preberemo iz spremenljivke "state") in zaženemo željeno funkcijo
  if state == "splash" then
    -- če je stanje "splash" zaženemo funkcijo za izris "splash" komponent
    splash.draw()
  elseif state == "game" then
    -- če je stanje "game" zaženemo funkcijo za izris "game" komponent in s tem zaženemo glavno igro
    game.draw()
  end
end

-- funkcija ki šteje čas sa nadaljno uporabo
function love.update(dt) -- =dt = delta time
  -- =call the state's update function
  -- funkcija ki posodobi čas
  if state == "splash" then
    -- če je stanje "splash" se posodobi časovna spremenljivka ki pripada "splash" datoteki
    splash.update(dt)
  elseif state == "game" then
    -- če je stanje "game" se posodobi časovna spremenljivka ki pripada "game" datoteki
    game.update(dt)
  end
end

-- funkcija, ki ves čas gleda ali je pritisnjena določena tipka na tipkovnici
function love.keypressed(key)
  -- =call the state's keypressed function
  -- če je stanje "splash" posreduje pritisnjene tipke v funkcijo "keypressed" z argumentom "key", katera se nahaja v datoteki "splash.lua"
  if state == "splash" then
    splash.keypressed(key)
  -- če je stanje "game" posreduje pritisnjene tipke v funkcijo "keypressed" z argumentom "key", katera se nahaja v datoteki "game.lua"
  elseif state == "game" then
    game.keypressed(key)
  end
  -- če je pritisnjena tipka "D" spremeni spremenljivko "debug", ki je zapisana kot boolean v njeno nasprotje
  if key == "d" then
    debug = not debug
  end
end
