game = {}

function game.load()
  -- =background init
  -- definirana spremenljivka za beleženje časa
  game.clock = 0
  
  -- =enemy init
  -- definirane spremenljivke za sovražnike
  game.enemy_size = imgs["enemy"]:getWidth() -- slika sovražnika
  game.enemies = {} -- spremenljivka, ki drži vse sovražnike
  game.enemy_dt = 0 -- časovna spremenljivka za sovražnike
  game.enemy_rate = 2 -- spremenljivka za število hitrosti pojavljanja sovražnikov

  -- =player init
  -- definirane spremenljivke za igralca
  game.player_size = imgs["player"]:getWidth() -- slika igralca
  game.playerx = 68*scale -- pozicijska koordinata x za igralca
  game.playery = 720*scale/2 -- pozicijska koordinata y za igralca

  -- =bullet init
  -- definirane spremenljivke za naboje
  game.ammo = 10 -- število nabojev
  game.recharge_dt = 0 -- časovna spremenljivka za naboje
  game.recharge_rate = 1 -- čas, ki mora poteči preden igralec dobi nov naboj
  game.bullet_size = imgs["bullet"]:getWidth() -- slika naboja
  game.bullets = {} -- spremenljivka, ki drži vse (leteče) naboje

  -- =info init
  -- ostale spremenljivke
  game.score = 0 -- spremenljivka z začetno vrednostjo osvojenih točk
  
  -- priklic zvoka v igri
  love.audio.stop() -- ustavi zvok, ki trenutno igra
  love.audio.play(music.game) -- predvajaj zvok "music.game"
end

-- funkcija za izris vseh elementov igre
function game.draw()
  -- =draw moving background
  -- narišemo poln pravokotnik čez celo okno
  love.graphics.draw(imgs["background"], 0, 0, 0, scale, scale)

  -- =draw enemies
  -- funkcija za izris sovražnikov (ki so v spremenljivki game.enemies)
  for _,v in ipairs(game.enemies) do
    -- izris posameznega sovražnika
    love.graphics.draw(imgs["enemy"], v.x, v.y, 0, scale, scale, game.enemy_size/2, game.enemy_size/2)
    -- če je stanje "debug" vključeno izriši še krog okoli sovražnika
    if debug == true then love.graphics.circle("line", v.x, v.y, game.enemy_size/2*scale) end
  end
  
  -- =draw player
  -- ukaz za izris igralca
  love.graphics.draw(imgs["player"], game.playerx, game.playery, 0, scale, scale, game.player_size/2, game.player_size/2)
    -- če je stanje "debug" vključeno izriši še krog okoli igralca
  if debug == true then love.graphics.circle("line", game.playerx, game.playery, game.player_size/2*scale) end

  -- =draw bullets
  -- funkcija za izris izstrelkov (ki so v spremenljivki game.bullets)
  for _,v in ipairs(game.bullets) do
    -- izris posameznega izstrelka
    love.graphics.draw(imgs["bullet"], v.x, v.y, 0, scale, scale, game.bullet_size/2, game.bullet_size/2)
    -- če je stanje "debug" vključeno izriši še krog okoli izstrelka
    if debug == true then love.graphics.circle("line", v.x, v.y, game.bullet_size/2*scale) end
  end

  -- =draw game info
  -- ukazi za izris podaktov o igri
  -- nastavimo barvo za nadaljno uporabo
  love.graphics.setColor(fontcolor.r, fontcolor.g, fontcolor.b)
  -- izris doseženih točk v potekajoči igri
  love.graphics.printf("score:"..game.score.." ammo:"..game.ammo, 0, 0, love.graphics.getWidth(), "center")
  -- če je stanje "debug" izpiši dodatne informacije
  if debug == true then
    -- izriši trenutno število sovražnikov, trenutno število izstrelkov, hitrost pojavljanja sovražnikov in število slik na sekundo
    love.graphics.print("enemies:"..#game.enemies.."\nbullets:"..#game.bullets.."\nenemy_rate:"..game.enemy_rate.."\nFPS:"..love.timer.getFPS(), 0, 14*scale)
  end
  -- nastavimo barvo na belo
  love.graphics.setColor(255, 255, 255)
end

-- funkcija za merjenje časa
function game.update(dt)
  -- =clock for background
  -- posodobimo spremenljivko za čas
  game.clock = game.clock + dt

  -- =update game.enemies
  -- posodobi spremenljivko za čas pri game.enemy_dt
  game.enemy_dt = game.enemy_dt + dt
  -- =enemy spawn
  -- če je minilo dovolj časa od pojave zadnjega sovražnika se zaženejo naslednji ukazi
  if game.enemy_dt > game.enemy_rate then
    -- od spremenljivke za pojavljanje sovražnikov odštejemo hitrost pojavljanja sovražnikov
    game.enemy_dt = game.enemy_dt - game.enemy_rate
    -- povečamo spremenljivko za hitrost ponavljanja sovražnikov
    game.enemy_rate = game.enemy_rate - 0.01 * game.enemy_rate
    -- ustvarimo spremenljivko "enemy" tipa array
    local enemy = {}
    -- nastavimo x koordinato za novega sovražnika
    enemy.x = 1280*scale + game.enemy_size
    -- nastavimo y koordinato za novega sovražnika s pomočjo naključnostne funkcije iz knjižnice "math"
    enemy.y = math.random((game.enemy_size/2)*scale, ((720*scale)-(game.enemy_size/2))*scale)
    -- dodamo novega sovražnika iz lokalne v globalno tabelo sovražnikov
    table.insert(game.enemies, enemy)
  end
  -- =update enemy
  -- zanka za posodobitev x koordinat obstoječih sovražnikov
  for ei,ev in ipairs(game.enemies) do
    -- posodobimo koordinato x za zaporednega sovražnika (gre v levo)
    ev.x = ev.x - 200*dt*scale
    -- če je dani sovražnik presegel levo mejo nastavimo stanje igre na "splash"
    if ev.x < 65*scale + game.enemy_size/2 then
      state = "splash"
    end
    -- =if a player gets too close to enemy
    -- če je dani sovražnik presegel levo mejo se igra konča in nastavimo stanje igre na "splash"
    if game.dist(game.playerx, game.playery, ev.x, ev.y) < (12+8)*scale then
      splash.load()
      state = "splash"
    end
  end

  -- =update player movement
  -- ukazi za spreminjanje lokacije igralca
  -- če je pritisnjena tipka "arrow-down" spremeni y koordinato igralca nižke
  if love.keyboard.isDown("down") then
    game.playery = game.playery + 420*dt*scale
  end
  -- če je pritisnjena tipka "arrow-up" spremeni y koordinato igralca višje
  if love.keyboard.isDown("up") then
    game.playery = game.playery - 420*dt*scale
  end
  -- =keep player on the map
  -- če se hoče igralec premakniti nižje od konca zaslona mu to preprečimo
  if game.playery > 720*scale then
    game.playery = 720*scale
  end
  -- če se hoče igralec premakniti višje od konca zaslona mu to preprečimo
  if game.playery < 0 then
    game.playery = 0
  end

  -- =update bullets
  -- zanka za posodobitev x koordinat obstoječih izstrelkov
  for bi,bv in ipairs(game.bullets) do
    -- posodobimo koordinato x za zaporednega izstrelka (gre v desno)
    bv.x = bv.x + 500*dt*scale
    -- če je izstrelek zapustil vidni zaslon ga izbrišemo iz tabele
    if bv.x > 1280 then
      table.remove(game.bullets, bi)
    end
    -- =update bullets with game.enemies
    -- zanka za preverjanje zadetkov izstrelkov, ki gre čez vse obstoječe sovražnike
    for ei,ev in ipairs(game.enemies) do
      -- preverimo če je izstrelek zadel zaporednega sovražnika
      if game.dist(bv.x, bv.y, ev.x, ev.y) < (game.bullet_size/2+game.enemy_size/2)*scale then
        -- zaigraj zvok "explode"
        love.audio.play(explode)
        -- dodaj eno točko v spremenljivko "game.score"
        game.score = game.score + 1
        -- izbriši izstrelek iz globalne tabele izstrelkov
        table.remove(game.bullets, bi)
        -- izbriši sovražnika iz globalne tabele sovražnikov
        table.remove(game.enemies, ei)
      end
    end
  end
  -- =update player ammutinion
  -- ukazi za ponapolnitev izstrelkov na podlagi pretečenega časa
  -- posodobimo spremenljivko "game.recharge_dt" tako da prištejemo časovno razliko
  game.recharge_dt = game.recharge_dt + dt
  -- če je poteklo dovolj časa pred zadnjim novopridobljenim izstrelkom dodamo še enega
  if game.recharge_dt > game.recharge_rate then
    -- ponastavimo spremenljivko za beleženje časa med ponapolnitvijo izstrelkov
    game.recharge_dt = game.recharge_dt - game.recharge_rate
    -- dodamo en izstrelek k skupni vsoti (spremenljivka "game.ammo")
    game.ammo = game.ammo + 1
    -- če je izstrelkov več kot 10 se nastavi vrednost nazaj na 10
    if game.ammo > 10 then
      game.ammo = 10
    end
  end
end

-- funkcija za zaznavanje priisnjenih tipk
function game.keypressed(key)
  -- =shoot a bullet
  -- če je pritisnjena tipka "S" ustvari izstrelek
  if key == "s" and game.ammo > 0 then
    -- zaigraj zvok "shoot"
    love.audio.play(shoot)
    -- odstrani en izstrelek iz globelne spremenljivke za izstrelke
    game.ammo = game.ammo - 1
    -- ustvari spremenljivko za novo izstreljen izstrelek
    local bullet = {}
    -- nastavi x koordinato izstrelka na x koordinato igralca
    bullet.x = game.playerx
    -- nastavi y koordinato izstrelka na y koordinato igralca
    bullet.y = game.playery
    -- dodaj izstelek v tabelo 
    table.insert(game.bullets, bullet)
  end
end

-- =distance formula
-- funkcija za določanje oddaljenosti dveh točk v 2D prostoru
-- funkcija sprejme po dve koordinati dveh predmetov
function game.dist(x1, y1, x2, y2)
  -- v formulo za izračun oddaljenosti dveh točk vstavimo sprejete argumente in rezultat vrnemo iz funkcije
  return math.sqrt((x1 - x2)^2 + (y1 - y2)^2)
end
