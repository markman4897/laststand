-- =Game scale
-- definirane vse globalne nastavitve (spremenljivke) za igro
scale = 0.8
function love.conf(t)
  -- spremenljivka za naslov
  t.title = "Last Stand"
  -- spremenljivka za širino
  t.window.width = 1280*scale
  -- spremenljivka za dolžino
  t.window.height = 720*scale
end
